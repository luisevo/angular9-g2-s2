import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './components/products/products.component';
import { PortalComponent } from './pages/portal/portal.component';
import { DocumentationComponent } from './pages/documentation/documentation.component';
import { DrawerContainerComponent } from './components/drawer-container/drawer-container.component';
import { DrawerComponent } from './components/drawer-container/drawer/drawer.component';
import { DrawerContentComponent } from './components/drawer-container/drawer-content/drawer-content.component';
import { DataBindingComponent } from './pages/documentation/sub-pages/binding/data-binding/data-binding.component';
import { TwoWayDataBindingComponent } from './pages/documentation/sub-pages/binding/two-way-data-binding/two-way-data-binding.component';
import { AttributeDirectivesComponent } from './pages/documentation/sub-pages/directives/attribute-directives/attribute-directives.component';
import { StructuralDirectivesComponent } from './pages/documentation/sub-pages/directives/structural-directives/structural-directives.component';
import { PipesComponent } from './pages/documentation/sub-pages/pipes/pipes.component';


// Configuración de lenguaje
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { DefaultImgPipe } from './pipes/default-img.pipe';

registerLocaleData(localeEs);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    NotFoundComponent,
    ProductComponent,
    ProductsComponent,
    PortalComponent,
    DocumentationComponent,
    DrawerContainerComponent,
    DrawerComponent,
    DrawerContentComponent,

    DataBindingComponent,
    TwoWayDataBindingComponent,
    AttributeDirectivesComponent,
    StructuralDirectivesComponent,
    PipesComponent,
    DefaultImgPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    // Configurar lenguaje global
    {
      provide: LOCALE_ID,
      useValue: 'es'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
