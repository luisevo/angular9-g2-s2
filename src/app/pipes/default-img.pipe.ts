import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'defaultImg'
})
export class DefaultImgPipe implements PipeTransform {

  transform(url: string, img = 'assets/img-not-found.png'): string {
    return url ? url : img;
  }

}
