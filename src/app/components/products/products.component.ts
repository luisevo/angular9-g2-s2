import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: string[] = [
    'tv',
    'dvd'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
