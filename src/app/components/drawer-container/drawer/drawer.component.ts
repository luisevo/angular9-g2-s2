import { Component, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.scss']
})
export class DrawerComponent {
  @Input() open = true;

  @HostBinding('class')
  get myClass() {
    return this.open ? 'active' : '';
  }

  toggle() {
    this.open = !this.open;
  }
}
