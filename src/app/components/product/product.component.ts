import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  name: string;

  products: string[] = [
    'blueray',
  ];

  subscription: Subscription;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  back() {
    this.router.navigateByUrl('/productos'); // sin parametros
    // this.router.navigate([]); con parametros
  }

  ngOnInit(): void {
    // this.name = this.activatedRoute.snapshot.paramMap.get('nombre');

    this.subscription = this.activatedRoute.paramMap.subscribe((params) => {
      this.name = params.get('nombre');
    });

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
