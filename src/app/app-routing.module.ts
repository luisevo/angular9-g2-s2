import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { ProductComponent } from './components/product/product.component';
import { ProductsComponent } from './components/products/products.component';
import { PortalComponent } from './pages/portal/portal.component';
import { DocumentationComponent } from './pages/documentation/documentation.component';
import { DataBindingComponent } from './pages/documentation/sub-pages/binding/data-binding/data-binding.component';
import { AttributeDirectivesComponent } from './pages/documentation/sub-pages/directives/attribute-directives/attribute-directives.component';
import { StructuralDirectivesComponent } from './pages/documentation/sub-pages/directives/structural-directives/structural-directives.component';
import { PipesComponent } from './pages/documentation/sub-pages/pipes/pipes.component';


const routes: Routes = [
  { path: '', redirectTo: 'documentacion', pathMatch: 'full' },
  {
    path: 'portal',
    component: PortalComponent,
    children: [
      { path: 'inicio', component: HomeComponent },
      { path: 'contactenos', component: ContactComponent },
    ]
  },

  { path: 'productos', component: ProductsComponent },
  { path: 'producto/:nombre', component: ProductComponent },

  {
    path: 'documentacion',
    component: DocumentationComponent,
    children: [
      { path: '', redirectTo: 'data-binding', pathMatch: 'full' },
      { path: 'data-binding', component: DataBindingComponent },
      { path: 'attribute-directives', component: AttributeDirectivesComponent },
      { path: 'structural-directives', component: StructuralDirectivesComponent },
      { path: 'pipes', component: PipesComponent },
    ]
  },

  // { path: '**', redirectTo: '' }
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
