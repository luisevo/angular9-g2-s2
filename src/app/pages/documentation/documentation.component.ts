import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-documentation',
  templateUrl: './documentation.component.html',
  styleUrls: ['./documentation.component.scss']
})
export class DocumentationComponent implements OnInit {

  menu: any[] = [
    { url: '/documentacion/data-binding', title: 'Data Binding' },
    { url: '/documentacion/attribute-directives', title: 'Directivas de Atributo' },
    { url: '/documentacion/structural-directives', title: 'Directivas Estructurales' },
    { url: '/documentacion/pipes', title: 'Pipes' },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
