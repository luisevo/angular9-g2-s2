import { Component, OnInit } from '@angular/core';

interface IPerson {
  name: string;
  lastname: string;
  age?: number;
}

@Component({
  selector: 'app-structural-directives',
  templateUrl: './structural-directives.component.html',
  styleUrls: ['./structural-directives.component.css']
})
export class StructuralDirectivesComponent implements OnInit {

  show: boolean = true;
  tasks: string[] = [
    'Tarea 1',
    'Tarea 2',
    'Tarea 3',
  ]
  people: any[] = [
    { name: 'Luis', lastname: 'Vilcarromero'},
    { name: 'Jose', lastname: 'Osorio', age: 30},
    null,
    { name: 'Miguel', lastname: 'Perez'},
  ]

  value: number;

  constructor() { }

  ngOnInit() {
  }

  toggleShow() {
    this.show = !this.show;
  }

  setValue(value: number) {
    this.value = value;
  }

}