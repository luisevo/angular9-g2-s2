import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attribute-directives',
  templateUrl: './attribute-directives.component.html',
  styleUrls: ['./attribute-directives.component.css']
})
export class AttributeDirectivesComponent implements OnInit {
  active: boolean;
  color: string = 'red';

  constructor() { }

  ngOnInit() {
  }

  toggleActive() {
    this.active = !this.active;
  }

  toggleColor() {
    this.color = (this.color === 'red') ? 'green' : 'red';
  }

}