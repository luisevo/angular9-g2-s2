import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-two-way-data-binding',
  templateUrl: './two-way-data-binding.component.html',
  styleUrls: ['./two-way-data-binding.component.css']
})
export class TwoWayDataBindingComponent implements OnInit {

  @Input() value: string = '';
  @Output() valueChange: EventEmitter<string> = new EventEmitter<string>(); 

  constructor() { }

  ngOnInit() {
  }

  click(event) {
    this.valueChange.emit(event.target.value);
  }

}