import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.css']
})
export class DataBindingComponent implements OnInit {

  stringInterpolation = '{{ }}';
  propertyBinding = '[ ]';
  eventBinding = '( )';
  twoWayDataBinding = '[( )]';
  helloWorld = 'Hola Mundo';

  constructor() { }

  ngOnInit() {
  }

  changeText() {
    this.helloWorld = 'Click';
  }

}