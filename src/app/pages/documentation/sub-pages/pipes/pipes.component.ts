import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipes',
  templateUrl: './pipes.component.html',
  styleUrls: ['./pipes.component.scss']
})
export class PipesComponent implements OnInit {

  text = 'Angular nueve';

  textList: string[] = [ 'elem 1', 'elem 2', 'elem 3', 'elem 4' ];

  price = 1999.99;

  loading = 0.58;

  person = {
    name: 'Luis'
  };

  now = new Date();

  generateFilePromise: Promise<string>;

  url = 'https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png';

  constructor() {
    this.generateFilePromise = new Promise((resolve, reject) => {
      try {
        setTimeout(() => {
          resolve('//documents/file.txt');
        }, 2000);
      } catch (e) {
        reject();
      }
    });
  }

  async ngOnInit() {

    /*
    const onSuccess = () => {  };
    const onError = () => {  };

    const generateFile = (onSuccess: () => void, onError: () => void) => {
      try {
        // Instrucciones
        onSuccess();
      } catch (e) {
        onError();
      }
    };


    generateFile(onSuccess, onError);
    */



    console.log('Start');

    await this.generateFilePromise
    .then( async (urlFile) => {
      console.log('onSuccess 1', urlFile);

      await new Promise((resolve) => {
        setTimeout(() => {
          resolve('hola mundo');
        }, 2000);
      })
      .then(message => console.log(message));

      return urlFile;
    })
    .then((urlFile) => {
      console.log('onSuccess 2', urlFile);
    })
    .catch(() => { console.log('onError'); });

    console.log('End');



  }

}
