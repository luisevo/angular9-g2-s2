import { Component, OnInit, SimpleChanges, OnChanges, OnDestroy, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit, OnChanges, OnDestroy {
  @Input() value: number = 0;
  @Output() log: EventEmitter<string> = new EventEmitter<string>();

  constructor() {
    this.log.emit('constructor');
  }

  add() {
    this.value += 1;
  }

  less() {
    this.value -= 1;
  }

  ngOnInit() {
    this.log.emit('ngOnInit');
  }

  ngOnChanges(changes: SimpleChanges) {
    this.log.emit(`ngOnChanges ${JSON.stringify(changes)}`);
  }

  ngOnDestroy() {
    this.log.emit('ngOnDestroy');
  }

}