import { Component } from '@angular/core';

@Component({
  selector: 'app-life-cycle-hooks',
  templateUrl: './life-cycle-hooks.component.html',
  styleUrls: ['./life-cycle-hooks.component.css']
})
export class LifeCycleHooksComponent {

  logs: string[] = [];

  show = true;
  count = 2;

  constructor() {
  }

  addCount() {
    this.count += 1;
  }

  addLog(log: string) {
    this.logs.push(log);
  }

  toggleShow() {
    this.show = !this.show;
  }

  

}