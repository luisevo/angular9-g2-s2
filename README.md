# ANGULAR 9: Pipes y Rutas

## Instalar dependencias

`npm install`

## Iniciar servidor

Ejecutar `ng serve` y abrir `http://localhost:4200/`

## Generar componentes

Ejecutar `ng generate component component-name` 

## Generar pipes

Ejecutar `ng generate pipe pipe-name` 
